from django.urls import path


from . import views


app_name = 'recruitments'

urlpatterns = [
    path('create/', views.CreateRecruitment.as_view(), name='create_recruitment'),

    path('list/', views.RecruitmentList.as_view(), name='recruitment_list'),

    path('create-question/', views.CreateQuestion.as_view(), name='create_question'),

    path('question-list/', views.QuestionList.as_view(), name='question_list'),

    path('start-test/', views.StartTest.as_view(), name='start_test'),

    path('answer-question/', views.AnswerQuestion.as_view(), name='answer_question'),

    path('finish-test/', views.FinishTest.as_view(), name='finish_test'),

    path('results/', views.Results.as_view(), name='results'),

    path('result/', views.Result.as_view(), name='result'),

    path('analyse/', views.AnalyseResult.as_view(), name='analyse'),

    path('graph/', views.DisplayGraph.as_view(), name='graph'),

]
