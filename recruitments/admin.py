from django.contrib import admin
from recruitments.models import Recruitment, Question, RecruitmentAttempt, RecruitmentAttemptResponse


admin.site.register([Recruitment, Question, RecruitmentAttemptResponse, RecruitmentAttempt])
# Register your models here.
