from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.TextField()
    subject = models.CharField(max_length=100)
    choice_1 = models.CharField(max_length=100)
    choice_2 = models.CharField(max_length=100)
    choice_3 = models.CharField(max_length=100)
    choice_4 = models.CharField(max_length=100)
    correct_answer = models.CharField(max_length=100)

    def __str__(self):
        return '{}, {}'.format(self.question_text, self.subject)


class Recruitment(models.Model):
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True, default='')
    experience = models.TextField(max_length=100, null=True)
    qualification = models.TextField(max_length=50, null=True)
    created_on = models.DateField(auto_now_add=True)
    expires_on = models.DateField()
    number_of_questions = models.PositiveIntegerField()
    question = models.ManyToManyField(Question, )
    correct_answer_score = models.IntegerField(default=0)
    incorrect_answer_score = models.IntegerField(default=0)

    @property
    def total_score(self):
        return self.correct_answer_score * self.number_of_questions

    def __str__(self):
        return self.name


class RecruitmentAttempt(models.Model):
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    recruitment = models.ForeignKey(Recruitment, on_delete=models.CASCADE)

    @property
    def correct_count(self):
        return self.recruitmentattemptresponse_set.filter(is_correct=True).exclude(question__subject='eq').count()

    @property
    def incorrect_count(self):
        return self.recruitmentattemptresponse_set.filter(is_correct=False).exclude(question__subject='eq').count()

    @property
    def score(self):
        return ((self.correct_count * self.recruitment.correct_answer_score) -
                (self.incorrect_count * self.recruitment.incorrect_answer_score))

    @property
    def eq_score(self):
        eq_qstns = self.recruitmentattemptresponse_set.filter(question__subject='eq')
        lis = []
        for qstn in eq_qstns:
            try:
                lis.append(int(qstn.response))
            except ValueError:
                lis.append(1)
        return sum(lis)

    @property
    def total_score(self):
        return self.eq_score + self.score

    @property
    def subject_score(self):
        subject_list = self.recruitmentattemptresponse_set.all().values_list('question__subject', flat=True)
        subjects = list(set(subject_list))
        marks = []
        for subject in subjects:
            mark = self.recruitmentattemptresponse_set.filter(question__subject=subject, is_correct=True).count()
            marks.append(mark * self.recruitment.correct_answer_score)
        return subjects, marks

    def __str__(self):
        return '{}, {}'.format(self.user, self.recruitment)


class RecruitmentAttemptResponse(models.Model):
    recruitment_attempt = models.ForeignKey(RecruitmentAttempt, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    response = models.TextField()
    is_correct = models.BooleanField(default=False)
