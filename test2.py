import csv
from recruitments.models import RecruitmentAttempt
results = RecruitmentAttempt.objects.all()
from copy import deepcopy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

f1 = [data.subject_score[1][0] for data in results]
f2 = [data.subject_score[1][1] for data in results]
print(f1, f2)
marks_array = np.array(list(zip(f1, f2)))
plt.scatter(f1, f2, c='black', s=7)
# Euclidean Distance Caculator
def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)



k = 3
# X coordinates of random centroids
c_x = np.random.randint(1, np.max(marks_array) - 5, size=k)
# Y coordinates of random centroids
c_y = np.random.randint(1, np.max(marks_array) - 5, size=k)
# centroid = np.array(list(zip(c_x, c_y)), dtype=np.float32)
centroid = [(1, 1), (2, 2), (3, 3)]
print('centroid', centroid)
c_old = np.zeros(centroid.shape)
# Cluster Lables(0, 1, 2)
clusters = np.zeros(len(marks_array))
# Error func. - Distance between new centroids and old centroids
error = dist(centroid, c_old, None)
print(error)

while error != 0:

    if str(centroid[0])=='nan' or str[error] == 'nan':
        kmeans = KMeans(n_clusters=3)
        # Fitting the input data
        kmeans = kmeans.fit(marks_array)
        # Getting the cluster labels
        labels = kmeans.predict(marks_array)
        # Centroid values
        centroid = kmeans.cluster_centers_
        break
    # Assigning each value to its closest cluster
    for i in range(len(marks_array)):
        distances = dist(marks_array[i], centroid)
        cluster = np.argmin(distances)
        clusters[i] = cluster
    # Storing the old centroid values
    C_old = deepcopy(centroid)
    print('C_old', c_old)
    # Finding the new centroids by taking the average value
    for i in range(k):
        points = [marks_array[j] for j in range(len(marks_array)) if clusters[j] == i]
        centroid[i] = np.mean(points, axis=0)
        print("c[i]", centroid[i])
    print("c again", centroid)
    error = dist(centroid, c_old, None)
    marks_array = input('hello')
    print(error)


